const assert = require('assert');
const Calculator = require('../src/calculator');

describe('Calculator', function () {
  beforeEach(function () {
    this.calc = new Calculator();
  });
  describe('#add()', function () {
    it('should return 2 with term 1 and current value 1', function () {
      this.calc.add(1);
      this.calc.add(1);
      assert.strictEqual(this.calc.getResult(), 2);
    });
    it('should throw errors while adding decimal', function () {
      assert.throws(() => this.calc.add(1.5), Error('Decimal numbers not allowed'));
    });
  });
  describe('#subtract()', function () {
    it('should return 0 with subtraction of 1 and current value 1', function () {
      this.calc.add(1);
      this.calc.subtract(1);
      assert.strictEqual(this.calc.getResult(), 0);
    });
  });
  describe('#multiply()', function () {
    it('should return 1 with factor 1 and current value 1', function () {
      this.calc.add(1);
      this.calc.multiply(1);
      assert.strictEqual(this.calc.getResult(), 1);
    });
    it('should return 0 with factor 0 and current value 1', function () {
      this.calc.add(1);
      this.calc.multiply(0);
      assert.strictEqual(this.calc.getResult(), 0);
    });
    it('should return -2 with factor -1 and current value 2', function () {
      this.calc.add(2);
      this.calc.multiply(-1);
      assert.strictEqual(this.calc.getResult(), -2);
    });
  });
});
