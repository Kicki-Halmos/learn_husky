const assert = require('assert');
// const Calculator = require('../src/calculator');

/* describe('Array', function() {
    before(function() {
        // runs once before the first test in this block
        console.log('before array');
      });
  describe('#indexOf()', function() {

      before(function() {
        // runs once before the first test in this block
        console.log('before');
      });

      after(function() {
        // runs once after the last test in this block
        console.log('after');
      });

      beforeEach(function() {
        // runs before each test in this block
        console.log('before each');
      });

      afterEach(function() {
        // runs after each test in this block
        console.log('after each');
      });

      it('should return -1 when the value is not present', function() {
        assert.strictEqual([1, 2, 3].indexOf(4), -1);
      });

      it('should return 1 when the value is 1', function() {
          assert.strictEqual([1, 2, 3].indexOf(1), 0);
        });

  });
});

describe('hooks', function(){
    it('should return true if number is number', function() {
        assert.ok(typeof (123 === 123));
      });
}) */

// var calculator = require('src/calc');

function add(x, y) {
  return x + y;
}

function multi(x, y) {
  return x * y;
}
describe('Calculator', function () {
  describe('add()', function () {
    it('Should return 3 when adding 1 and 2', function () {
      assert.strictEqual(add(1, 2), 3);
    });
    it('Should return -2 when adding -1 and -1', function () {
      assert.strictEqual(add(-1, -1), -2);
    });
    it('Should return 0 when adding -1 and 1', function () {
      assert.strictEqual(add(-1, 1), 0);
    });
    it('Should return 1 when multiplying 1 and 1', function () {
      assert.strictEqual(multi(1, 1), 1);
    });
    it('Should return 0 when multiplying 1 and 0', function () {
      assert.strictEqual(multi(1, 0), 0);
    });
    it('Should return 0 when multiplying 0 and 1', function () {
      assert.strictEqual(multi(0, 1), 0);
    });
    it('Should return 4 when multiplying 2 and 2', function () {
      assert.strictEqual(multi(0, 1), 0);
    });
    it('Should return -4 when multiplying -2 and 2', function () {
      assert.strictEqual(multi(0, 1), 0);
    });
    it('Should return 4 when multiplying -2 and -2', function () {
      assert.strictEqual(multi(0, 1), 0);
    });
  });
});
